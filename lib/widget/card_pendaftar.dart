import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:url_launcher/url_launcher.dart';
import '/page/detail_project.dart';

class CardPendaftar extends StatelessWidget {
  final String name;
  final String description;
  final String kontak;
  final String cv;

  CardPendaftar(this.name, this.description, this.kontak, this.cv);

  _showSimpleModalDialog(context){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:BorderRadius.circular(10.0)),
            child: SizedBox(
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Nama",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              this.name,
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Deskripsi",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              this.description,
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Kontak",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              this.kontak,
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Link CV",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: GestureDetector(
                              onTap: () async {
                                var url = this.cv;
                                if (await canLaunch(url)) {
                                  await launch(url);
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(content: Text("Tidak bisa membuka link")),
                                  );
                                }
                              },
                              child: Text(
                                "Buka disini",
                                style: TextStyle(color: Colors.blue),
                                textAlign: TextAlign.left,
                              ),
                            )
                          ),
                          OutlinedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text("Tutup"),
                            style: OutlinedButton.styleFrom(
                              primary: Colors.red[400],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _showSimpleModalDialog(context);
      },
      child: SizedBox(
        width: 400,
        child: Card(
          margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: new Color(0xE9E9E9E9),
              width: 1.0,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          child: InkWell(
            splashColor: Colors.blue[100],
            onTap: () {
              _showSimpleModalDialog(context);
            },
            child: Padding(
              padding: EdgeInsets.fromLTRB(40, 30, 40, 30),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
                    child: Text(
                      name,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    child: Text(description),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
