import 'dart:convert';
import 'dart:core';

class Project {
  final int idProject;
  final String namaProject;

  final int idPemilik;
  final String namaPemilik;

  final String kategori;
  final String bayaran;
  final String deskripsi;
  final String estimasi;
  final String skills;
  final String kontakPemilik;
  final String jumlahPelamar;
  final bool acc;
  final bool buka;

  Project({
    this.idProject,
    this.namaProject,
    this.idPemilik,
    this.namaPemilik,
    this.kategori,
    this.bayaran,
    this.deskripsi,
    this.estimasi,
    this.skills,
    this.kontakPemilik,
    this.jumlahPelamar,
    this.acc = false,
    this.buka = true,
  });

  Map<String, dynamic> toMap() {
    return {
      'idProject': idProject,
      'namaProject': namaProject,
      'idPemilik': idPemilik,
      'namaPemilik': namaPemilik,
      'kategori': kategori,
      'bayaran': bayaran,
      'deskripsi': deskripsi,
      'estimasi': estimasi,
      'skills': skills,
      'kontakPemilik': kontakPemilik,
      'jumlahPelamar': jumlahPelamar,
      'acc': acc,
      'buka': buka,
    };
  }

  factory Project.fromJson(Map<String, dynamic> map) {
    return Project(
      idProject: map['idProject'],
      namaProject: map['namaProject'],
      idPemilik: map['idPemilik'],
      namaPemilik: map['namaPemilik'],
      kategori: map['kategori'],
      bayaran: map['bayaran'],
      deskripsi: map['deskripsi'],
      estimasi: map['estimasi'],
      skills: map['skills'],
      kontakPemilik: map['kontakPemilik'],
      jumlahPelamar: map['jumlahPelamar'],
      acc: map['acc'],
      buka: map['buka'],
    );
  }

  String toJson() => json.encode(toMap());

  @override
  String toString() {
    return 'Project(idProject: $idProject, namaProject: $namaProject, idPemilik: $idPemilik, namaPemilik: $namaPemilik, kategori: $kategori, bayaran: $bayaran, deskripsi: $deskripsi, estimasi: $estimasi, skills: $skills, kontakPemilik: $kontakPemilik, jumlahPelamar: $jumlahPelamar, acc: $acc, buka: $buka)';
  }
}
