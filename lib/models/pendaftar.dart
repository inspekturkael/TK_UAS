class Pendaftar {
  final String name;
  final String description;
  final String kontak;
  final String cv;
  final int id_project;
  final String id_user;

  const Pendaftar({
    this.name,
    this.description,
    this.kontak,
    this.cv,
    this.id_project,
    this.id_user,
  });

  factory Pendaftar.fromJson(Map<String, dynamic> json) {
    return Pendaftar(
      name: json['nama_pelamar'],
      description: json['pesan'],
      kontak: json['kontak_pelamar'],
      cv: json['cv_pelamar'],
      id_project: json['id_project'],
      id_user: json['id_user'],
    );
  }
}
