import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:tkuas/models/question.dart';
import 'package:tkuas/widget/navigation_widget.dart';
import 'package:http/http.dart' as http;

Future<List<Question>> fetchQuestion() async {
  final response = await http.get(Uri.parse('https://project-channel.herokuapp.com/faq/mobile_json/'));
  print(response);
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return parseItem(response.body);
  } else {
    // If the server did not return a 200 OK response,d
    // then throw an exception.
    throw Exception('Failed to load Get');
  }
}

List<Question> parseItem(String responseBody) {
  final List<Question> questions = [];
  final parsed = jsonDecode(responseBody) as List<dynamic>;
  for (var e in parsed) {
    questions.add(Question.fromJson(e));
  }
  return questions;
}
Future<List<Question>> futureQuestion = fetchQuestion();

class FaqListScreen extends StatelessWidget {
  static const routeName = 'faq/';

  @override
  Widget build(BuildContext context) {
    return FutureBuilder (
      future: futureQuestion,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Scaffold(
            appBar: AppBar(title: Text('Frequently Asked Question'),),
            drawer: NavigationDrawerWidget(),
            body: Center(
              child: Text('Tidak ada FAQ'),
            )
          );
        } else if (snapshot.hasData) {
          var data = snapshot.data as List<Question>;
          List<MyTile> listOfTiles = <MyTile> [
            for(var q in data)
              new MyTile(q.fields.question ,
                  <MyTile>[
                    new MyTile(q.fields.answer)
                  ])
          ];
          return Scaffold(
            appBar: AppBar(title: Text('Frequently Asked Question'),),
            body: SingleChildScrollView(
              controller: ScrollController(),
              physics: ScrollPhysics(),
              child: Column(
                children: <Widget>[
                  Text(""),
                  Center(
                    child: Text(
                      "FAQ",
                      style: TextStyle(
                        fontSize: 40.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    padding: const EdgeInsets.all(15.0),
                    itemBuilder: (BuildContext context, int index) {
                      return new StuffInTiles(listOfTiles[index]);
                    },
                    itemCount: listOfTiles.length,),
                  Center(
                    child: FormScreen(),
                  )
                ],
              ),
            ),
            drawer: NavigationDrawerWidget(),
          );
        }
        return const CircularProgressIndicator();
      },
    );
  }
}
class FormScreen extends StatelessWidget {
  String question = '';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: 20.0),
        Center(
          child: Container(
              padding: EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0))),
              child: new Center (
                  child: Text(
                    "Ada Pertanyaan?",
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  )
              )
          ),
        ),

        SizedBox(height: 20.0),

        new Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsetsDirectional.only(top: 25.0, bottom: 8.0, start: 16.0, end: 16.0),
            child: Text(
              "Pertanyaan:",
              softWrap: true,
              style: new TextStyle(
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold,
                  ),
            ),
          ),
        ),

        Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  onChanged: (String value) {
                    question = value;
                  },
                  maxLines: 7,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Tuliskan pertanyaan Anda...",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 0.3, color: Colors.black),
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                  ),
                )
              ],
            ),

          ),
        ),

        SizedBox(height: 20.0),
        MaterialButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            padding: EdgeInsets.all(15.0),

            color: Color(0xffffffff),
            hoverColor: Color(0xff8aa3a5),

            onPressed: () async {
              final response = await http.post(Uri.parse('https://project-channel.herokuapp.com/faq/mobile_json/'),
                  headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8'
                  },
                  body: jsonEncode(<String, String>{
                    'question': question,
                    'answer': "",
                  }));
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => FaqListScreen()),
              );
              futureQuestion = fetchQuestion();
            },
            child: Text(" Kirimkan pertanyaan ", style: TextStyle(
              // fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 17.0
            ))
        ),
        SizedBox(height: 20.0),
      ],
    );
  }
}

class StuffInTiles extends StatelessWidget {
  final MyTile myTile;
  StuffInTiles(this.myTile);

  @override
  Widget build(BuildContext context) {
    return _buildTiles(myTile);
  }

  Widget _buildTiles(MyTile t) {
    if (t.children.isEmpty)
      return new ListTile(
          dense: true,
          enabled: true,
          isThreeLine: false,
          onLongPress: () => print("long press"),
          onTap: () => print("tap"),
          title: new Text(t.title, style: TextStyle(fontSize: 16.0)));

    return new ExpansionTile(
      key: new PageStorageKey<int>(3),
      backgroundColor: Colors.white70,
      collapsedBackgroundColor: Colors.white70,
      title: new Text(t.title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0)),
      children: t.children.map(_buildTiles).toList(),
    );
  }
}

class MyTile {
  String title;
  List<MyTile> children;
  MyTile(this.title, [this.children = const <MyTile>[]]);
}