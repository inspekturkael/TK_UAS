import 'package:flutter/material.dart';
import '/widget/navigation_widget.dart';
import '/page/profile_page.dart';

import 'package:provider/provider.dart';
import 'package:tkuas/page/login_page.dart';
import 'package:tkuas/utils/cookie_request.dart';

import 'dart:async';
import 'dart:core';
import 'dart:convert';
import 'package:http/http.dart' as http;
/*
Future<User> fetchUser(String id) async {
  const url = 'http://127.0.0.1:8000/buat_profile/id';
  try {
    final response = await http.get(Uri.parse(url));
    print(response);
    print(response.body);
    Map<String, dynamic> data = jsonDecode(response.body);
    print(data);
   
    return User.fromJson(jsonDecode(response.body));
      
  } catch (error) {
    print("error");
  }
}
Future<User> updateUser(String nama, String jenis_kelamin, String institusi, String kontak, String email) async {
  const url = 'http://127.0.0.1:8000/buat_profile/3/edit';
  try {
    print("yahyaaa");
    final response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'nama' : nama,
        'jenis_kelamin' : jenis_kelamin,
        'institusi' : institusi,
        'kontak' : kontak,
        'email' : email,
      }),
    );
    print("tesss");
    print(response);
    print(response.body);
    Map<String, dynamic> data = jsonDecode(response.body);
    print(data);
   
    return User.fromJson(jsonDecode(response.body));
      
  } catch (error) {
    print("error");
  }
} */
class User {
    String nama = "";
    String jenis_kelamin = "";
    String institusi = "";
    String kontak = "";
    String email = "";

    User({
      this.nama,
      this.jenis_kelamin,
      this.institusi,
      this.kontak,
      this.email
    });

    factory User.fromJson(Map<String, dynamic> data) {
      return User(
        nama : data["data"]["data"]["nama"],
        jenis_kelamin : data["data"]["data"]["jenis_kelamin"],
        institusi : data["data"]["data"]["institusi"],
        kontak : data["data"]["data"]["kontak"],
        email : data["data"]["data"]["email"]
      );
    }
}

class EditProfile extends StatefulWidget {
  final String id;
  EditProfile(this.id) : super(key: null);

  @override
  _EditProfile createState() => _EditProfile();
}
class _EditProfile extends State<EditProfile> {
  final TextEditingController _controllerNama = TextEditingController();
  final TextEditingController _controllerJenisKelamin = TextEditingController();
  final TextEditingController _controllerInstitusi = TextEditingController();
  final TextEditingController _controllerKontak = TextEditingController();
  final TextEditingController _controllerEmail = TextEditingController();
  
  //Future<User> futureUser =  fetchUser(user_id);


  String validasi_email ="aman";

   void createAlertDialog() {
    showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        title: Text("Maaf atas ketidaknyamanannya"),
        content: Text("Email ini sudah digunakan User lain"),
        actions: <Widget>[
          FlatButton(onPressed: Navigator.of(context).pop, child: Text("Close")),
        ],
      );
    });
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawerWidget(),
      appBar: AppBar(
        title: Text("Edit Profile"),
      ),
      body: SingleChildScrollView(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
            ),
            child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              "Edit Profil",
              style: TextStyle(
                color: Colors.purple,
                fontSize: 30,
                fontWeight: FontWeight.w500,
                ),
              ),
            )
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(55),
            child: Image(
              width: 110.0,
              height: 110.0,
              image: NetworkImage(
                'https://apkplz.net/storage/images/big/profile/picture/instagram/downloader/big.profile.picture.instagram.downloader_1.png'),
            ),
          ),
          Padding(padding: EdgeInsets.all(8.0)),
          FractionallySizedBox(
          
            widthFactor: 0.7,
            child: Form(
            key: _formKey,
            child: Container(
            padding : EdgeInsets.all(30.0), 
            decoration:  BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Colors.purple, Colors.blue]),
              //color: Color.fromARGB(100,214, 177, 227),
              borderRadius: BorderRadius.circular(10.0),
            ),
             child: Column(
              children: [
                TextFormField(  
                  style: TextStyle(color: Colors.white),
                  controller: _controllerNama,
                  decoration: const InputDecoration(  
                    icon: const Icon(Icons.person),  
                    hintText: 'Enter your name ...',  
                    labelText: 'Nama', 
                    hintStyle: TextStyle(color : Colors.white) ,
                    labelStyle: TextStyle(color : Colors.white) ,
                    
                  ),
                  validator: (value) {
                      if (value.isEmpty) {
                        return 'Fill the input field ';
                      }
                      return null;
                    },
                ),
   
                TextFormField( 
                  style: TextStyle(color: Colors.white),
                  controller: _controllerJenisKelamin, 
                  decoration: const InputDecoration(  
                    icon: const Icon(Icons.male),  
                    hintText: 'Enter your gender ...',  
                    labelText: 'Jenis Kelamin', 
                    hintStyle: TextStyle(color : Colors.white) ,
                    labelStyle: TextStyle(color : Colors.white) , 
                  ),
                  validator: (value) {
                      if (value.isEmpty) {
                        return 'Fill the input field ';
                      }
                      return null;
                    },
                ),

                TextFormField(  
                  style: TextStyle(color: Colors.white),
                  controller: _controllerInstitusi,
                  decoration: const InputDecoration(  
                    icon: const Icon(Icons.home),  
                    hintText: 'Enter your institution ...',  
                    labelText: 'Asal Institusi',
                    hintStyle: TextStyle(color : Colors.white) ,
                    labelStyle: TextStyle(color : Colors.white) ,  
                  ),
                  validator: (value) {
                      if (value.isEmpty) {
                        return 'Fill the input field ';
                      }
                      return null;
                    },
                ),
                TextFormField(  
                  style: TextStyle(color: Colors.white),
                  controller: _controllerKontak,
                  decoration: const InputDecoration(  
                    icon: const Icon(Icons.phone),  
                    hintText: 'Enter your contact number ...',  
                    labelText: 'Kontak',
                    hintStyle: TextStyle(color : Colors.white) ,
                    labelStyle: TextStyle(color : Colors.white) ,  
                  ),
                  validator: (value) {
                      if (value.isEmpty) {
                        return 'Fill the input field ';
                      }
                      return null;
                    },
                ),
                TextFormField(  
                  style: TextStyle(color: Colors.white),
                  controller: _controllerEmail,
                  decoration: const InputDecoration(  
                    icon: const Icon(Icons.email),  
                    hintText: 'Enter your email ...',  
                    labelText: 'Email',  
                    hintStyle: TextStyle(color : Colors.white) ,
                    labelStyle: TextStyle(color : Colors.white) ,
                  ),
                  validator: (value) {
                      if (value.isEmpty) {
                        return 'Fill the input field ';
                      }
                      return null;
                    },
                ),
              ],
            ),
            ),
          ),
          ),
         
          FractionallySizedBox(
            widthFactor: 0.7,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [ 
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: RaisedButton(
                    color: Colors.black,
                    child: Text("BACK", style:  TextStyle(fontSize: 15.0, color: Colors.white)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: RaisedButton(
                    color: Colors.purple,
                    child: Text("SUBMIT", style:  TextStyle(fontSize: 15.0,color: Colors.white)),
                    onPressed: () async {
                      String id = widget.id;
                      print(id);
                      //var URL = "http://127.0.0.1:8000/buat_profile/" + id + "/validate_email_flutter";
                      var URL = "https://project-channel.herokuapp.com/buat_profile/" + id + "/validate_email_flutter";
                      final Response = await http.post(
                          Uri.parse(URL),
                          headers: <String, String>{
                            'Content-Type': 'application/json'
                          },
                          body: jsonEncode(<String, String>{
                            'email' : _controllerEmail.text,
                          }),
                        );
                        print(Response);
                        print(URL);
                        print(Response.body);
                        Map<String, dynamic> data = jsonDecode(Response.body);
                        print("tesss1111");
                        validasi_email = data['data']['validasi'];
                        print(validasi_email);
                        print("tesss");
                      if (validasi_email != "terpakai" ) {
                        if (_formKey.currentState.validate()) {
                          print(id);
                          //var url = 'http://127.0.0.1:8000/buat_profile/'+ id + '/edit';
                          var url = 'https://project-channel.herokuapp.com/buat_profile/'+ id + '/edit';

                          print("yahyaaa");
                          final response = await http.post(
                            Uri.parse(url),
                            headers: <String, String>{
                              'Content-Type': 'application/json'
                            },
                            body: jsonEncode(<String, String>{
                              'nama' : _controllerNama.text,
                              'jenis_kelamin' : _controllerJenisKelamin.text,
                              'institusi' : _controllerInstitusi.text,
                              'kontak' : _controllerKontak.text,
                              'email' : _controllerEmail.text,
                            }),
                          );
                          print("tesss");
                          print(response);
                          print(response.body);
                          Map<String, dynamic> data = jsonDecode(response.body);
                          print(data);
    
                          Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProfilePage(id)),
                          );

                          /*
                          setState(() {
                            futureUser = updateUser(_controllerNama.text, _controllerJenisKelamin.text, _controllerInstitusi.text, _controllerKontak.text, _controllerEmail.text);
                          }); */
                        }
                      }
                      else {
                        print(validasi_email);
                        createAlertDialog();
                        
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
       ),
      ),
    );
  }
}