import 'package:flutter/material.dart';
import 'package:tkuas/models/pendaftar.dart';

import '/widget/navigation_widget.dart';
import '/widget/card_pendaftar.dart';
import 'mendaftar_page.dart';
import 'edit_project.dart';

import 'package:provider/provider.dart';
import 'package:tkuas/page/login_page.dart';
import 'package:tkuas/utils/cookie_request.dart';

import 'package:responsive_grid_list/responsive_grid_list.dart';

import 'dart:async';
import 'dart:core';
import 'dart:convert';
import 'package:http/http.dart' as http;

class DetailProjectPage extends StatefulWidget {
  //final int id = 6;
  final int id;
  DetailProjectPage(this.id) : super(key: null);

  DetailProject createState()=> DetailProject();
}

class DetailProject extends State<DetailProjectPage> {
  //var id = widget.id;
  bool isScreenWide = false;
  var buttons = <Widget>[];

  CookieRequest request = CookieRequest();
  var user_id = "";
  bool loggedIn = false;
  bool admin = false;

  bool isPelamar = false;
  bool viewMelamar = false;
  List<Pendaftar> pendaftar = [];
  Future<Map<String, dynamic>> futureData;
  Map<String, dynamic> data = {'data' : {'data': null}};

  _showSimpleModalDialog(context, String condition){
    var id = widget.id;
    var url;
    var text;
    if(condition == "Tutup"){
      url = "https://project-channel.herokuapp.com/project/detail/closeproject/$id/flutter";
      text = "Apakah anda ingin menutup proyek ini?";
    }
    else{
      url = "https://project-channel.herokuapp.com/project/detail/deleteproject/$id/flutter";
      text = "Apakah anda ingin menghapus proyek ini?";
    }
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:BorderRadius.circular(10.0)),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      text,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.black,
                          wordSpacing: 1
                          )
                      ),
                    Container(
                      margin: const EdgeInsets.only(top: 40.0),
                      child:
                      Flex(
                        direction: isScreenWide ? Axis.horizontal : Axis.vertical,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ElevatedButton(
                            onPressed: () async {
                              var response;
                              Map<String, dynamic> map;
                              try{
                                response = await http.post(
                                  Uri.parse(url),
                                  headers: <String, String>{
                                    'Content-Type': 'application/json'
                                  },
                                );
                                map = jsonDecode(response.body);
                                if(map['instance'] == "Proyek Ditutup"){
                                  buttons.removeLast();
                                  buttons.removeLast();
                                  setState(() {});
                                }
                                else if(map['instance'] == "Proyek Dihapus"){
                                  Navigator.of(context).pop();
                                }
                                Navigator.of(context).pop();
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text(map['instance'])),
                                );
                              } catch (error){
                                print(error);
                              }
                            },
                            child: Text(condition),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.red[400],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 20.0),
                            child: OutlinedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text("Kembali"),
                              style: OutlinedButton.styleFrom(
                                primary: Colors.red[400],
                              ),
                            ),
                          ),
                        ],
                      )
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  dynamic defineButton(var user_id, var admin, bool isScreenWide) {
    int id = widget.id;
    List<Widget> buttons = <Widget>[];
    Widget editButton = Container(
      margin: isScreenWide ? EdgeInsets.all(7.5) : EdgeInsets.all(5),
      child: OutlinedButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => EditProjectPage(id),
          ));
        },
        child: Text("Edit Proyek"),
        style: OutlinedButton.styleFrom(
          primary: Colors.blue[400],
        ),
      ),
    );
    Widget closeButton = Container(
      margin: isScreenWide ? EdgeInsets.all(7.5) : EdgeInsets.all(5),
      child: ElevatedButton(
        onPressed: () {
          _showSimpleModalDialog(context, "Tutup");
        },
        child: Text("Tutup Proyek"),
        style: ElevatedButton.styleFrom(
          primary: Colors.red[400],
        ),
      ),
    );
    Widget lamarButton = Container(
      margin: isScreenWide ? EdgeInsets.all(7.5) : EdgeInsets.all(5),
      child: ElevatedButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => MendaftarPage(user_id, id),
          ));
        },
        child: Text("Lamar"),
        style: ElevatedButton.styleFrom(
          primary: Colors.blue[400],
        ),
      ),
    );
    Widget tolakButton = Container(
      margin: isScreenWide ? EdgeInsets.all(7.5) : EdgeInsets.all(5),
      child: OutlinedButton(
        onPressed: () {
          _showSimpleModalDialog(context, "Hapus");
        },
        child: Text("Tolak"),
        style: OutlinedButton.styleFrom(
          primary: Colors.blue[400],
        ),
      ),
    );
    Widget approveButton = Container(
      margin: isScreenWide ? EdgeInsets.all(7.5) : EdgeInsets.all(5),
      child: ElevatedButton(
        onPressed: () async {
          var response;
          Map<String, dynamic> map;
          try{
            response = await http.post(
                Uri.parse("https://project-channel.herokuapp.com/project/detail/approveproject/$id/flutter"),
                headers: <String, String>{
                  'Content-Type': 'application/json'
                },
                body: jsonEncode(<String, dynamic>{
                  'username': user_id
                })
            );
            map = jsonDecode(response.body);
            buttons.removeLast();
            buttons.removeLast();
            if(map['instance'] == 1){
              buttons.add(closeButton);
              buttons.add(editButton);
              setState(() {
                viewMelamar = true;
              });
            }
            else{
              buttons.add(lamarButton);
              buttons.add(closeButton);
              setState(() {
                viewMelamar = true;
              });
            }
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("Proyek telah disetujui")),
            );
          } catch (error){
            print(error);
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("Proyek gagal disetujui")),
            );
          }
        },
        child: Text("Setujui"),
        style: ElevatedButton.styleFrom(
          primary: Colors.blue[400],
        ),
      ),
    );

    var pemilik = data['data']['data']['id_pemilik'];
    bool acc = data['data']['data']['acc'];
    bool buka = data['data']['data']['buka'];
    for(int i = 0 ; i < data['data']['data_lamar'].length ; i++){
      if(data['data']['data_lamar'][i]['id_user'] == pemilik){
        isPelamar = true;
        break;
      }
    }

    if(admin){
      if(!buka){
        setState(() {
          viewMelamar = true;
        });
      }
      else if(user_id == pemilik && acc){
        setState(() {
          viewMelamar = true;
          buttons.add(closeButton);
          buttons.add(editButton);
        });
      }
      else if(!acc){
        setState(() {
          buttons.add(approveButton);
          buttons.add(tolakButton);
        });
      }
      else if(isPelamar){
        setState(() {
          viewMelamar = true;
          buttons.add(closeButton);
        });
      }
      else{
        setState(() {
          viewMelamar = true;
          buttons.add(lamarButton);
          buttons.add(closeButton);
        });
      }
    }
    else{
      if(isPelamar || user_id == null){}
      else if(user_id == pemilik){
        if(!acc){
          setState(() {
            buttons.add(editButton);
          });
        }
        else{
          setState(() {
            viewMelamar = true;
            buttons.add(closeButton);
            buttons.add(editButton);
          });
        }
      }
      else{
        setState(() {
          buttons.add(lamarButton);
        });
      }
    }

    return buttons;
  }

  Future<Map<String, dynamic>> fetchedData(bool isScreenWide) async {
    var id = widget.id;
    var response;
    Map<String, dynamic> map;
    try{
      response = await http.get(
        Uri.parse("https://project-channel.herokuapp.com/project/detail/$id/flutter"),
      );
      map = jsonDecode(response.body);
      data = map;
      if(data['status'] != 200){
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(data['data'])),
        );
        Navigator.of(context).pop();
      }
      for(int i = 0 ; i < data['data']['data_lamar'].length; i++){
        pendaftar.add(Pendaftar.fromJson(data['data']['data_lamar'][i]));
      }

      setState(() {
        buttons = defineButton(user_id, admin, isScreenWide);
      });
      return map;
    } catch (error){
      print(error);
      return map;
    }
  }

  @override
  void initState(){
    super.initState();
    fetchedData(isScreenWide);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final _request = Provider.of<CookieRequest>(context, listen: false);

      if (!_request.loggedIn) {
        Navigator.of(context).pop();
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => LoginPage(),
        ));
      } else {
        setState(() {
          request = _request;
          loggedIn = _request.loggedIn;
          user_id = _request.username;
          admin = _request.is_admin;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    isScreenWide = MediaQuery.of(context).size.width >= 480;

    //test();

    return Scaffold(
      appBar: AppBar(
        title: Text(
            'Detail Project',
        ),
      ),
      body: SingleChildScrollView(
        controller: ScrollController(),
        child:Center(
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.80,
                margin: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 20.0),
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 40.0),
                child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 20.0),
                        child: Text(
                          data['data']['data'] != null ? data['data']['data']['nama_project'] : "Nama Project",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              letterSpacing: 1.2,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                        child: Text(
                          data['data']['data'] != null ? data['data']['data']['deskripsi'] : "Deskripsi",
                          style: TextStyle(
                            height: 1.6,
                          ),
                        ),
                      ),
                      Flex(
                        direction: isScreenWide ? Axis.horizontal : Axis.vertical,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: buttons,
                      )
                    ]),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.80,
                margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
                padding: EdgeInsets.fromLTRB(10.0, 40.0, 10.0, 20.0),
                alignment: Alignment.centerLeft,
                child: Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                        child: Text(
                          data['data']['data'] != null ? data['data']['data']['kategori'] : "Kategori",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              letterSpacing: 0.5,
                              fontSize: 14,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Pemilik proyek",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              data['data']['data'] != null ? data['data']['data']['nama_pemilik'] : "Nama Pemilik",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Bayaran",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              data['data']['data'] != null ? "Rp" + data['data']['data']['bayaran'].toString() : "Rp0",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Skill yang dibutuhkan",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              data['data']['data'] != null ? data['data']['data']['skills'] : "Skills",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Jumlah orang yang dibutuhkan",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              data['data']['data'] != null ? data['data']['data']['jumlah_orang'].toString() + " Orang" : "0 Orang",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Estimasi waktu",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              data['data']['data'] != null ? data['data']['data']['estimasi'] : "Estimasi",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Kontak",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              data['data']['data'] != null ? data['data']['data']['kontak_pemilik'] : "Kontak Pemilik",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                    ]),
              ),
              viewMelamar ? Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.80,
                margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                      child: Text(
                        "Daftar Pelamar",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            letterSpacing: 1.2,
                            fontSize: 18,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    pendaftar.length > 0 ?
                    ResponsiveGridList(
                      horizontalGridSpacing: 16, // Horizontal space between grid items
                      verticalGridSpacing: 16, // Vertical space between grid items
                      horizontalGridMargin: 10, // Horizontal space around the grid
                      verticalGridMargin: 50, // Vertical space around the grid
                      minItemWidth: 200, // The minimum item width (can be smaller, if the layout constraints are smaller)
                      maxItemsPerRow: 5, // The maximum items to show in a single row. Can be useful on large screens
                      shrinkWrap: true, // shrinkWrap property of the ListView.builder()
                      children: pendaftar.map((data) => CardPendaftar(data.name, data.description, data.kontak, data.cv)).toList(),
                    ) :
                    Container(
                      margin: EdgeInsets.only(top:20.0, bottom:20.0),
                      child: Text(
                        "Belum ada pelamar yang mendaftar di proyek ini",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ) : Container()
            ]
          )
        ),
      ),
    );
  }
}
