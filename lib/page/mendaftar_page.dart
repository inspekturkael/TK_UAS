// import dari Flutter
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

// import dari lokal
import '../widget/navigation_widget.dart';

class MendaftarPage extends StatefulWidget {
  //@override
  final String idUser;
  final int idProject;
  MendaftarPage(this.idUser, this.idProject) : super(key: null);
  _MendaftarPageState createState() => _MendaftarPageState();
}

class _MendaftarPageState extends State<MendaftarPage> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController namaPelamarField = TextEditingController();
  TextEditingController pesanField = TextEditingController();
  TextEditingController kontakPelamarField = TextEditingController();
  TextEditingController cvPelamarField = TextEditingController();

  Future<void> submit(
      BuildContext context, String _idUser, int _idProject) async {
    int idProject = _idProject;
    String idUser = _idUser;
    final response = await http.post(
        Uri.parse(
            "https://project-channel.herokuapp.com/add_lamar/lamar/$idProject/lamar_flutter"),
        headers: <String, String>{'Content-Type': 'application/json'},
        body: jsonEncode(<String, dynamic>{
          'namaPelamar': namaPelamarField.text,
          'pesan': pesanField.text,
          'kontakPelamar': kontakPelamarField.text,
          'cvPelamar': cvPelamarField.text,
          'idUser': idUser,
          'idProject': idProject,
        }));
    print(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawerWidget(),
      appBar: AppBar(
        title: Text("Silahkan isi form berikut ini"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: namaPelamarField,
                    decoration: new InputDecoration(
                      hintText: "Isi dengan nama sesuai identitas",
                      labelText: "Nama Pelamar",
                      icon: Icon(Icons.person),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: pesanField,
                    decoration: new InputDecoration(
                      hintText: "Isi pesan anda kepada pembuat proyek",
                      labelText: "Pesan Pelamar",
                      icon: Icon(Icons.email),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Pesan tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: kontakPelamarField,
                    decoration: new InputDecoration(
                      hintText: "format {jenis_kontak} : {id/nomor akun}",
                      labelText: "Kontak Pelamar",
                      icon: Icon(Icons.phone),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Kontak tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: cvPelamarField,
                    decoration: new InputDecoration(
                      hintText:
                          "cantumkan link file cv yang sudah diupload di drive",
                      labelText: "CV Pelamar",
                      icon: Icon(Icons.add_to_drive),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'CV tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Row(
                  children: <Widget>[
                    const Text(
                        '\n*Unggah File CV Anda ke Drive kemudian Masukkan Link Menuju File Tersebut\n',
                        style: TextStyle(color: Colors.red)),
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    if (_formKey.currentState?.validate() ?? true) {
                      showConfirmDialog(
                          context, widget.idUser, widget.idProject);
                      // updateUI();
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showConfirmDialog(BuildContext context, String idUser, int idProject) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    Widget continueButton = TextButton(
      child: Text("Confirm"),
      onPressed: () {
        submit(context, idUser, idProject);
        int count = 0;
        Navigator.of(context).popUntil((_) => count++ >= 1);
        showAlertDialog(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("AlertDialog"),
      content:
          Text("Apakah Anda Yakin Informasi yang Anda Berikan Sudah Sesuai?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        int count = 0;
        Navigator.of(context).popUntil((_) => count++ >= 1);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Selamat"),
      content: Text("Lamaran berhasil dikirim"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
