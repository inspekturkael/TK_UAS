// import dari Flutter
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

// import dari lokal
import '../widget/navigation_widget.dart';

class BuatProjectPage extends StatefulWidget {
  final String id;
  BuatProjectPage(this.id) : super(key: null);

  @override
  _BuatProjectPageState createState() => _BuatProjectPageState();
}

class _BuatProjectPageState extends State<BuatProjectPage> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController namaProjectField = TextEditingController(text: "");

  TextEditingController namaPemilikField = TextEditingController(text: "");

  TextEditingController kategoriField = TextEditingController(text: "");
  TextEditingController bayaranField = TextEditingController(text: ""); //(int)
  TextEditingController deskripsiField = TextEditingController(text: "");
  TextEditingController estimasiField = TextEditingController(text: "");
  TextEditingController skillsField = TextEditingController(text: "");
  TextEditingController kontakPemilikField = TextEditingController(text: "");
  TextEditingController jumlahPelamarField = TextEditingController(text: ""); 

  Future<void> submit(BuildContext context, String idUser) async {
    String idPemilik = idUser;
    final response = await http.post(
        Uri.parse(
            "https://project-channel.herokuapp.com/buat_project/post_flutter/" +
                idPemilik),
        headers: <String, String>{'Content-Type': 'application/json'},
        body: jsonEncode(<String, dynamic>{
          'namaPemilik': namaPemilikField.text,
          'namaProject': namaProjectField.text,
          'kategori': kategoriField.text,
          'bayaran': bayaranField.text,
          'deskripsi': deskripsiField.text,
          'estimasi': estimasiField.text,
          'skills': skillsField.text,
          'kontakPemilik': kontakPemilikField.text,
          'jumlahPelamar': jumlahPelamarField.text,
          'idPemilik': int.parse(idPemilik),
        }));
    print(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawerWidget(),
      appBar: AppBar(
        title: Text("Halaman Buat Project"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: namaProjectField,
                    decoration: new InputDecoration(
                      hintText: "KitaBantu",
                      labelText: "Nama Proyek",
                      icon: Icon(Icons.assignment),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Nama Proyek tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: namaPemilikField,
                    decoration: new InputDecoration(
                      hintText: "Mikael",
                      labelText: "Nama Pemilik",
                      icon: Icon(Icons.person),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Nama Pemilik tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: kategoriField,
                    decoration: new InputDecoration(
                      hintText: "Web Development",
                      labelText: "Kategori Proyek",
                      icon: Icon(Ionicons.briefcase),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Kategori tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: bayaranField,
                    decoration: new InputDecoration(
                      hintText: "1.000.000.000 (tanpa titik)",
                      labelText: "Bayaran",
                      icon: Icon(Icons.attach_money),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Bayaran tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: deskripsiField,
                    decoration: new InputDecoration(
                      labelText: "Deskripsi Proyek",
                      icon: Icon(Ionicons.book_outline),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Deskripsi tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: estimasiField,
                    decoration: new InputDecoration(
                      hintText: "2 Tahun",
                      labelText: "Estimasi Waktu",
                      icon: Icon(Icons.timer),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Estimasi Waktu tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: skillsField,
                    decoration: new InputDecoration(
                      hintText: "Python",
                      labelText: "Daftar Skill yang Dibutuhkan",
                      icon: Icon(Ionicons.construct_outline),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Daftar Skill tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: kontakPemilikField,
                    decoration: new InputDecoration(
                      hintText: "0821-xxxx-xxxx",
                      labelText: "Kontak Pemilik",
                      icon: Icon(Ionicons.id_card_outline),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Kontak tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: jumlahPelamarField,
                    decoration: new InputDecoration(
                      hintText: "100",
                      labelText: "Jumlah Orang",
                      icon: Icon(Icons.people_outline),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Jumlah Orang tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    if (_formKey.currentState?.validate() ?? true) {
                      String id = widget.id;
                      showConfirmDialog(context, id);
                      // updateUI();
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showConfirmDialog(BuildContext context, String idUser) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Tidak jadi"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    Widget continueButton = TextButton(
      child: Text("Iya"),
      onPressed: () {
        submit(context, idUser);
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => BuatProjectPage(idUser)));
        showAlertDialog(context, idUser);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("AlertDialog"),
      content: Text("Apakah Anda Yakin ini Project yang Anda ingin buat?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAlertDialog(BuildContext context, String idUser) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => BuatProjectPage(idUser)));
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Selamat!"),
      content: Text("Proyek Berhasil Dibuat!"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
