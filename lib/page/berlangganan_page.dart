import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkuas/utils/cookie_request.dart';
import '/widget/navigation_widget.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

import 'daftar_proyek_page.dart';

class BerlanggananPage extends StatefulWidget {
  @override
  _BerlanggananPageState createState() => _BerlanggananPageState();
}

class _BerlanggananPageState extends State<BerlanggananPage> {
  final _formKey = GlobalKey<FormState>();

  CookieRequest request = CookieRequest();
  bool loggedIn;
  int id = 0;
  String username = "";
  String email = "";
  bool is_admin = false;
  bool is_subscribed = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final _request = Provider.of<CookieRequest>(context, listen: false);

      if (!_request.loggedIn || _request.is_subscribed) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => DaftarProyekPage(),
        ));
      } else {
        setState(() {
          request = _request;
          loggedIn = _request.loggedIn;
          id = _request.id;
          username = _request.username;
          email = _request.email;
          is_admin = _request.is_admin;
          is_subscribed = _request.is_subscribed;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavigationDrawerWidget(),
        appBar: AppBar(
          // The title text which will be shown on the action bar
          title: Text("Berlangganan"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Tertarik untuk Berlangganan?"),
              SizedBox(height: 24),
              Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                      controller: ScrollController(),
                      child: ElevatedButton(
                          child: Text("Langganan"),
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                          ),
                          onPressed: () async {
                            final response = await http.post(
                                Uri.parse(
                                    "https://project-channel.herokuapp.com/subscribe/"),
                                headers: <String, String>{
                                  'Content-Type': 'application/json'
                                },
                                body: jsonEncode(<String, String>{
                                  'email': email,
                                }));
                            if (json.decode(response.body)['message'] ==
                                'Terima kasih telah berlangganan!') {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text(
                                        'Terima kasih telah berlangganan!')),
                              );
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content:
                                        Text('Kamu gagal berlangganan :(')),
                              );
                            }
                            startTimer();
                          })))
            ],
          ),
        ));
  }

  Timer _timer;
  int _start = 5;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
            request.berlangganan();
            Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => DaftarProyekPage(),
            ));
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
