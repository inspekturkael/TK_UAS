import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'detail_project.dart';

import 'dart:async';
import 'dart:core';
import 'dart:convert';
import 'package:http/http.dart' as http;

/*
void main() {
  runApp(MaterialApp(
    title: "Edit Project",
    theme: ThemeData(
      scaffoldBackgroundColor: const Color(0xF8F8F8F8),
      primarySwatch: Colors.blue,
    ),
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}
*/
class EditProjectPage extends StatefulWidget{
  final int id;
  EditProjectPage(this.id) : super(key: null);

  EditProject createState()=> EditProject();
}

class EditProject extends State<EditProjectPage> {
  final _formKey = GlobalKey<FormState>();
  var edited = 0.0 ;
  var firstValue = {};
  TextEditingController nama_pemilik = TextEditingController(text: "");
  TextEditingController nama_project = TextEditingController(text: "") ;
  TextEditingController kategori = TextEditingController(text: "") ;
  TextEditingController bayaran = TextEditingController(text: "") ;
  TextEditingController deskripsi = TextEditingController(text: "") ;
  TextEditingController estimasi = TextEditingController(text: "") ;
  TextEditingController skills = TextEditingController(text: "") ;
  TextEditingController kontak_pemilik = TextEditingController(text: "") ;
  TextEditingController jumlah_orang = TextEditingController(text: "") ;
  Map<String, dynamic> data;

  void fetchData() async {
    var response;
    Map<String, dynamic> map;
    var id = widget.id;
    try{
      response = await http.get(
        Uri.parse("https://project-channel.herokuapp.com/project/detail/$id/edit/flutter"),
        headers: <String, String>{
          'Content-Type': 'application/json;'
        },
      );

      map = jsonDecode(response.body);
      data = map['data']['data'];
      setState(() {
        nama_pemilik.text = data['nama_pemilik'];
        nama_project.text = data['nama_project'];
        kategori.text = data['kategori'];
        bayaran.text = data['bayaran'].toString();
        deskripsi.text = data['deskripsi'];
        estimasi.text = data['estimasi'];
        skills.text = data['skills'];
        kontak_pemilik.text = data['kontak_pemilik'];
        jumlah_orang.text = data['jumlah_orang'].toString();
      });
    } catch (error){
      print(error);
    }
  }

  @override
  void initState(){
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    final id = widget.id;
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Project"),
      ),
      backgroundColor: Colors.white,
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.all(20.0),
                  child: Text(
                    "Edit Project",
                    style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(48.0, 30.0, 0.0, 0.0),
                  child: Text(
                    "Nama Pemilik",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: nama_pemilik,
                    decoration: new InputDecoration(
                      hintText: "contoh: Muhammad Haqqi Al Farizi",
                      icon: Icon(Icons.people),
                      fillColor: const Color(0xF2F2F2F2),
                      filled: true,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0)
                      ),
                    ),
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Nama tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(48.0, 15.0, 0.0, 0.0),
                  child: Text(
                    "Nama Proyek",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: nama_project,
                    decoration: new InputDecoration(
                      hintText: "contoh: Kacamata Transparan",
                      icon: Icon(Icons.build),
                      fillColor: const Color(0xF2F2F2F2),
                      filled: true,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Nama proyek tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(48.0, 15.0, 0.0, 0.0),
                  child: Text(
                    "Kategori",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: kategori,
                    decoration: new InputDecoration(
                      hintText: "contoh: Web Development",
                      icon: Icon(Icons.category),
                      fillColor: const Color(0xF2F2F2F2),
                      filled: true,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Kategori tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(48.0, 15.0, 0.0, 0.0),
                  child: Text(
                    "Bayaran",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: bayaran,
                    decoration: new InputDecoration(
                      hintText: 'contoh: 100000',
                      icon: Icon(Icons.money),
                      fillColor: const Color(0xF2F2F2F2),
                      filled: true,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Bayaran tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(48.0, 15.0, 0.0, 0.0),
                  child: Text(
                    "Deskripsi",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 120.0,
                    child: TextFormField(
                      controller: deskripsi,
                      maxLines: 5,
                      decoration: new InputDecoration(
                        hintText: "contoh: Proyek ini adalah proyek yang ditujukan untuk membuat mahasiswa indonesia menjadi sehat mental",
                        icon: Icon(Icons.description),
                        fillColor: const Color(0xF2F2F2F2),
                        filled: true,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if(value != null){
                          if (value.isEmpty) {
                            return 'Deskripsi tidak boleh kosong';
                          }
                          return null;
                        }
                      },
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(48.0, 15.0, 0.0, 0.0),
                  child: Text(
                    "Estimasi Waktu",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: estimasi,
                    decoration: new InputDecoration(
                      hintText: "contoh: 9 Bulan",
                      icon: Icon(Icons.timer),
                      fillColor: const Color(0xF2F2F2F2),
                      filled: true,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Estimasi waktu tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(48.0, 15.0, 0.0, 0.0),
                  child: Text(
                    "Skill yang dibutuhkan",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 120.0,
                    child: TextFormField(
                      controller: skills,
                      maxLines: 5,
                      decoration: new InputDecoration(
                        hintText: "contoh: \n React, Flutter, Panjat Tebing, Struktur Data",
                        icon: Icon(Icons.handyman),
                        fillColor: const Color(0xF2F2F2F2),
                        filled: true,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if(value != null){
                          if (value.isEmpty) {
                            return 'Skill yang dibutuhkan tidak boleh kosong';
                          }
                          return null;
                        }
                      },
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(48.0, 15.0, 0.0, 0.0),
                  child: Text(
                    "Kontak",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 120.0,
                    child: TextFormField(
                      controller: kontak_pemilik,
                      maxLines: 5,
                      decoration: new InputDecoration(
                        hintText: "contoh: \n Gmail: abc@gmail.com \n Line: @sule_elus",
                        icon: Icon(Icons.contact_mail),
                        fillColor: const Color(0xF2F2F2F2),
                        filled: true,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if(value != null){
                          if (value.isEmpty) {
                            return 'Kontak pemilik tidak boleh kosong';
                          }
                          return null;
                        }
                      },
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(48.0, 15.0, 0.0, 0.0),
                  child: Text(
                    "Jumlah orang",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: jumlah_orang,
                    decoration: new InputDecoration(
                      hintText: 'contoh: 15',
                      icon: Icon(Icons.people),
                      fillColor: const Color(0xF2F2F2F2),
                      filled: true,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Jumlah orang tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  child: RaisedButton(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.blue,
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Mengedit Data')),
                        );
                        final response = await http.post(
                            Uri.parse("https://project-channel.herokuapp.com/project/detail/$id/edit/flutter"),
                            headers: <String, String>{
                              'Content-Type': 'application/json'
                            },
                            body: jsonEncode(<String, dynamic>{
                              'nama_pemilik': nama_pemilik.text,
                              'nama_project': nama_project.text,
                              'kategori': kategori.text,
                              'bayaran': bayaran.text,
                              'deskripsi': deskripsi.text,
                              'estimasi': estimasi.text,
                              'skills': skills.text,
                              'kontak_pemilik': kontak_pemilik.text,
                              'jumlah_orang': jumlah_orang.text,
                            })
                        );
                        Map<String, dynamic> map = jsonDecode(response.body);

                        setState(() {
                          edited = 1.0;
                          Navigator.of(context).pop();
                          Navigator.of(context).pop();
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => DetailProjectPage(id),
                          ));
                        });
                      }
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Opacity(
                      opacity: edited,
                      child: Text(
                        "Proyek berhasil diedit!",
                      ),
                    ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}