import 'package:flutter/material.dart';
import '/widget/navigation_widget.dart';
import '/page/edit_profile.dart';

import 'dart:async';
import 'dart:core';
import 'dart:convert';
import 'package:http/http.dart' as http;
/*
Future<void> fetchData(String nama ,String jenis_kelamin, String institusi, String kontak, String email ) async {
  const url = 'http://127.0.0.1:8000/buat_profile/3';
  try {
    final response = await http.get(Uri.parse(url));
    print(response);
    print(response.body);
    Map<String, dynamic> data = jsonDecode(response.body);
    print(data);
    //print(data["data"]["data"]["nama"]);
    nama = data["data"]["data"]["nama"];
    jenis_kelamin = data["data"]["data"]["jenis_kelamin"];
    institusi = data["data"]["data"]["institusi"];
    kontak = data["data"]["data"]["kontak"];
    email = data["data"]["data"]["email"];
    print(nama);
    print(jenis_kelamin);
    print(institusi);
    print(kontak);
    print(email);
      //Map<String, dynamic> extractedData = jsonDecode(response.body);
      //extractedData.forEach((key, val) {
        //print(val);
      //});
      
  } catch (error) {
    print("error");
  }
} */

Future<User> fetchUser(String ID) async {
  String id = ID;
  //var url = 'http://127.0.0.1:8000/buat_profile/' + id;
  var url = 'https://project-channel.herokuapp.com/buat_profile/' + id;
  try {
    final response = await http.get(Uri.parse(url));
    print(response);
    print("yahyaa");
    print(response.body);
    print(url);
;   Map<String, dynamic> data = jsonDecode(response.body);
    print("yahyaa");
    print(data);
   
    return User.fromJson(jsonDecode(response.body));
      
  } catch (error) {
    print("error");
  }
}

class User {
    String nama = "";
    String jenis_kelamin = "";
    String institusi = "";
    String kontak = "";
    String email = "";
    String id = "";
    User({
      this.nama,
      this.jenis_kelamin,
      this.institusi,
      this.kontak,
      this.email,
      this.id,
    });

    factory User.fromJson(Map<String, dynamic> data) {
      return User(
        nama : data["data"]["data"]["nama"],
        jenis_kelamin : data["data"]["data"]["jenis_kelamin"],
        institusi : data["data"]["data"]["institusi"],
        kontak : data["data"]["data"]["kontak"],
        email : data["data"]["data"]["email"],
        id : data['data']['data']["id"],
      );
    }
}

class ProfilePage extends StatefulWidget {
  final String id;
  ProfilePage(this.id) : super(key: null);

  @override
  _ProfilPage createState() => _ProfilPage();

}
class _ProfilPage extends State<ProfilePage> {
  //String id = widget.id; //masih error
  //Future<User> futureUser =  fetchUser(id);

  @override
  Widget build(BuildContext context) {
    /*
    print("hello");
    String nama = "";
    String jenis_kelamin = "";
    String institusi = "";
    String kontak = "";
    String email = "";
    /*
    setState(() {
      fetchData(nama, jenis_kelamin, institusi, kontak, email);
    }); */
    print(nama + "yahyaa");
    print(jenis_kelamin);
    print(institusi);
    print(kontak);
    print(email); */
    String id = widget.id;
    Future<User> futureUser =  fetchUser(id);
    return Scaffold(
      drawer: NavigationDrawerWidget(),
      appBar: AppBar(
        // The title text which will be shown on the action bar
        title: Text("Profile"),
      ),
      body: SingleChildScrollView(
        child:  Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
        
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
            ),
            child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              "Profil Anda",
              style: TextStyle(
                color: Colors.purple,
                fontSize: 30,
                fontWeight: FontWeight.w500,
                ),
              ),
            )
          ),  
          ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image(
              width: 120.0,
              height: 120.0,
              image: NetworkImage(
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRqLh8BfhOwnybidu25dp7u43KcUHoN9MNUIQ&usqp=CAU'),
            ),
          ),
          FractionallySizedBox(
            widthFactor: 0.7,
            //alignment: Alignment.topCenter,
            child: Container(
            alignment: Alignment.center,
            padding : EdgeInsets.all(20.0),
            decoration:  BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [Colors.purple, Colors.blue]
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  //margin: EdgeInsets.only(bottom: 5.0),
                  child: Text(
                    "Nama :",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                    ),
                ),   
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child : FutureBuilder<User>(
                      future: futureUser,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          //id = snapshot.data.id;
                          return Text(
                            snapshot.data.nama,
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.w300,
                              color : Colors.white,
                            ),
                          );
                        }
                        else if (snapshot.hasError) {
                          return Text("${snapshot.error}");
                        }
                        return CircularProgressIndicator(); 
                      },
                    ),
                    /*
                    child: Text(
                      nama,
                      style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.w300,
                      ),
                    ), */
                  ),    
                ), 
                Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: Text(
                    "Jenis Kelamin :",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                    ),
                ),  
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child : FutureBuilder<User>(
                      future: futureUser,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Text(
                            snapshot.data.jenis_kelamin,
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.w300,
                              color: Colors.white,
                            ),
                          );
                        } 
                        else if (snapshot.hasError) {
                          return Text("${snapshot.error}");
                        }
                        return CircularProgressIndicator(); 
                      },
                    ),
                  ), 
                ),  
                 
                Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: Text(
                    "Asal  Institusi :",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                    ),
                ),
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child : FutureBuilder<User>(
                      future: futureUser,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Text(
                            snapshot.data.institusi,
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.w300,
                              color: Colors.white,
                            ),
                          );
                        }
                        else if (snapshot.hasError) {
                          return Text("${snapshot.error}");
                        }
                        return CircularProgressIndicator(); 
                      },
                    ),
                  ),  
                ),
                  
                Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: Text(
                    "Kontak :",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                    ),
                ), 
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child : FutureBuilder<User>(
                      future: futureUser,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Text(
                            snapshot.data.kontak,
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.w300,
                              color: Colors.white,
                            ),
                          );
                        } 
                        else if (snapshot.hasError) {
                          return Text("${snapshot.error}");
                        }
                        return CircularProgressIndicator(); 
                      },
                    ),
                  ),  
                ),   
                Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: Text(
                    "Email :",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ),  
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child : Container(
                    padding: EdgeInsets.all(8.0),
                    
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child : FutureBuilder<User>(
                      future: futureUser,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Text(
                            snapshot.data.email,
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.w300,
                              color: Colors.white,
                            ),
                          );
                        } 
                        else if (snapshot.hasError) {
                          return Text("${snapshot.error}");
                        }
                        return CircularProgressIndicator(); 
                      },
                    ),
                  ),  
                ),  
                 
              ],
            ),
          ),
          ),
          FractionallySizedBox(
            widthFactor: 0.7,
            child: Container(
                  margin: EdgeInsets.only(top: 10.0),
                  color: Colors.lightBlue,
                  child: RaisedButton(
                    color: Colors.deepPurple,
                    child: Text("Edit", style: TextStyle(fontSize: 15.0, color: Colors.white)),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => EditProfile(id)),
                      );
                    }    
                  ), 
                ),
          ),
          
        ],
      ),
      ),
      
      /* Center(
        child: Text(
          'Ini Profile',
        ), 
      ),*/
    );
  }

}