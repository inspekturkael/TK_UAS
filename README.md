# C06 PAS PBP

Nama anggota:
- 2006596592	Mikael Alvian Rizky
- 2006596075	Muhammad Haqqi Al Farizi
- 2006596150	Mochammad Agus Yahya
- 2006595791	Dimas Ichsanul Arifin
- 2006596491	Charles Pramudana
- 2006596522	Gitan Sahl Tazakha Wijaya
- 2006595936	Jeremy Reeve Kurniawan

Link APK: https://ristek.link/ProjectChannelAPK

# Cerita Integrasi:
Pertama-tama kami menyesuaikan setting web Django kami agar dapat terkoneksi dengan aplikasi nanti. Setelah itu, kami mengimplementasikan widget flutter sesuai dengan desain website yang dikonversikan ke dalam tampilan mobile. Selanjutnya, kami juga mengimplementasikan JsonResponse atau JSON Serializer Django pada backend Django dengan membuat fungsi khusus untuk digunakan pada flutter sehingga datanya dapat diproses di aplikasi nanti. Kemudian, kami mengintegrasikan backend website dengan flutter dengan menggunakan konsep asynchronous HTTP. Terakhir, kami mengimplementasikan flutter support authentication dengan library Dart.

Daftar Modul:
1. FAQ (form nambahin FAQ (ada di tampilan admin)) ✓
    - load daftar FAQ (async)
    <br>

2. Daftar Project (Loading (get) Project, sekalian async) ✓
    - Berlangganan
    - List proyek pribadi
    - List proyek untuk admin
    - List proyek untuk umum
    <br>

3. Profile ✓
    - Tampilan profile user (async)
    - Edit profile user (async)
    <br>

4. Melamar ✓
    - Nama pelamar
    - Cover Letter
    - kontak pelamar yang dapat dihubungi
    - CV pelamar (dalam link)
    <br>

5. Halaman Project ✓
    - Detail Project
        - Data dari project tersebut
        - Daftar orang yang melamar project tersebut
    - Edit Project
    <br>

6. Buat Project (Form data project (async kirim ke daftar project)) (response JSON = notif) ✓
    - Nama project
    - kategori
    - bayaran
    - deskripsi
    - estimasi waktu
    - skill yang diminta
    - kontak klien
    - jumlah orang yang dibutuhkan
    <br>

7. Login/Logout (bikin buat user) + Sign up (Form data pengguna baru) ✓
    - Login:
        - email
        - password
    - Form sign up:
        - Username
        - Nama
        - Jenis kelamin
        - Asal institusi
        - Email
        - Password
        - Kontak yang dapat dihubungi 
    <br>

Peran-peran pengguna:
- User nonlogin:
    - Melihat Daftar Proyek
    - Melihat FAQ
    - Membuat akun
    <br>

- User login:
    - Sama seperti user nonlogin (kecuali membuat akun)
    - Membuat proyek
    - Melamar proyek
    - Mengedit profile
    - Melihat daftar pelamar pada proyek miliknya
    - Mengedit proyek miliknya
    - Menutup proyek miliknya
    <br>

- Admin:
    - Sama seperti user login
    - Menjawab FAQ
    - Melihat daftar pelamar pada semua proyek
    - Mengapprove proyek
    - Menutup semua proyek
    <br>
    
